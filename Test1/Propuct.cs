﻿
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Test12
{
    class Product
    {
        private int price;
        private String name;
        public static ArrayList cart = new ArrayList();

        public int getPrice()
        {
            return price;
        }
        public void setPrice(int price)
        {
            this.price = price;
        }

        public String getName()
        {
            return name;
        }

        public void setName(String name)
        {
            this.name = name;
        }

        public String getRecommendation()
        {
            if (price > 100)
            {
                return "แพง";
            }
            else if (price > 50)
            {
                return "ธรรมดา";
            }
            else
            {
                return "ถูก";
            }
        }

        public String getCategory()
        {

            if (name.StartsWith("ผัก"))
            {
                return "ผัก";
            }
            else
            {
                return "ไม่ผัก";
            }
        }
        public double getReducedPrice(String level)
        {
            switch (level)
            {
                case "high":
                    return price * 0.5;

                case "medium":
                    return price * 0.7;

                case "low":
                    return price * 0.9;
            }
            return price;
        }

        public void addToCart()
        {
            Product.cart.Add(this);
        }

        public void printAllProductInCart()
        {
            foreach (Product p in Product.cart)
            {
                Console.WriteLine("name " + p.getName());
            }
        }
    }
}
